const editorElement = document.getElementById('editor');
const resultElement = document.getElementById('result');
const undoElement = document.getElementById('undo');
const redoElement = document.getElementById('redo');
const clearElement = document.getElementById('clear');
const exportLatex = document.getElementById('export-latex');
const answerElement = document.getElementById('answer');
const resultPopupElement = document.getElementById('resultPopup');
const resultCalcElement = document.getElementById('resultCalc');
editorElement.addEventListener('changed', (event) => {
  undoElement.disabled = !event.detail.canUndo;
  redoElement.disabled = !event.detail.canRedo;
  clearElement.disabled = !event.detail.isEmpty;
});

editorElement.addEventListener('exported', (evt) => {
  if (evt.detail) {
    resultElement.innerHTML = '<span>' + JSON.stringify(evt.detail) + '</span>';
  } else {
    resultElement.innerHTML = '';
  }
});

function cleanLatex(latexExport) {
  if (latexExport.includes('\\\\')) {
    const steps = '\\begin{align*}' + latexExport + '\\end{align*}';
    return steps
      .replace('\\begin{aligned}', '')
      .replace('\\end{aligned}', '')
      .replace(new RegExp('(align.{1})', 'g'), 'aligned');
  }
  return latexExport.replace(new RegExp('(align.{1})', 'g'), 'aligned');
}

editorElement.addEventListener('exported', (evt) => {
  const exports = evt.detail.exports;
  if (exports && exports['application/x-latex']) {
    clearElement.disabled = false;
    answerElement.disabled = false;
    katex.render(cleanLatex(exports['application/x-latex']), resultElement);

    exportLatex.innerHTML =
      '<span>' + exports['application/x-latex'] + '</span>';
  } else if (exports && exports['application/mathml+xml']) {
    clearElement.disabled = false;
    answerElement.disabled = false;
    resultElement.innerText = exports['application/mathml+xml'];
  } else if (exports && exports['application/mathofficeXML']) {
    clearElement.disabled = false;
    answerElement.disabled = false;
    resultElement.innerText = exports['application/mathofficeXML'];
  } else {
    clearElement.disabled = true;
    answerElement.disabled = true;
    resultElement.innerHTML = '';
  }
});
undoElement.addEventListener('click', () => {
  editorElement.editor.undo();
});
redoElement.addEventListener('click', () => {
  editorElement.editor.redo();
});
clearElement.addEventListener('click', () => {
  editorElement.editor.clear();
  resultPopupElement.style.display = 'none';
});

answerElement.addEventListener('click', () => {
  editorElement.editor.clear();
});

function integral(a, b, n, f) {
  let s, d, xb, xe, x, t;
  s = 0;
  d = (b - a) / n;
  xb = a;
  t = f;
  f = 'with (Math) {' + f + '}';
  for (i = 0; i < n; i++) {
    xe = xb + d;
    x = (xb + xe) / 2;
    s = s + d * eval(f);
    xb = xe;
  } //Math.sign(s) * Math.round(Math.abs(s),-2)*(-1)
  t = resultElement.innerHTML + ' = ' + parseFloat(s.toFixed(1)) * -1;
  resultCalcElement.innerHTML = t;
}

$('.open-popup').click(function () {
  $('.popup-bg').fadeIn(600);
  $('.ms-editor').css('height', '0');
  console.log(exportLatex.innerHTML);
  let str = exportLatex.innerHTML;
  new_str = str.replace('<span>', '');
  str = new_str.replace('</span>', '');

  new_str = str.replace('int ^', '');
  str = new_str.replaceAll('{', '');
  new_str = str.replace('}', '');
  str = new_str.replace('dx', '');
  new_str = str.replace('\\', '');
  str = new_str;
  console.log(str);
  let a = '';
  let a1 = false,
    b1 = false;
  let b = '';
  let f = '';
  for (let i = 0; i < str.length; i++) {
    if (str[i] == '_') {
      a1 = true;
    } else {
      if (a1 == false) a += str[i];
      else {
        if (str[i] == '}') b1 = true;
        else {
          if (b1 == false) b += str[i];
          else {
            if (str[i] != 'x') f += str[i];
            else {
              if (!isNaN(str[i - 1]) && str[i] == 'x') f = f + '*' + str[i];
              else f += str[i]
            }
          }
        }
      }
    }
  }

  console.log(str);

  let n;

  a2 = parseInt(a);
  b2 = parseInt(b);
  n = 100;

  integral(a2, b2, n, f);

  exportLatex.innerHTML = '';
});

$('.close-popup').click(function () {
  $('.popup-bg').fadeOut(600);
  $('.ms-editor').css('height', 'calc(100vh - 340px)');
});

iink.register(editorElement, {
  recognitionParams: {
    type: 'MATH',
    protocol: 'WEBSOCKET',
    server: {
      scheme: 'https',
      host: 'webdemoapi.myscript.com',
      applicationKey: '6cd77b26-6c0e-4ecc-ac60-599e9bd96566',
      hmacKey: 'd0c7f52e-1d14-4e2d-875e-9c5cdf193ceb',
    },
    iink: {
      math: {
        mimeTypes: ['application/x-latex', 'application/vnd.myscript.jiix'],
      },
      export: {
        jiix: {
          strokes: true,
        },
      },
    },
  },
});

window.addEventListener('resize', () => {
  editorElement.editor.resize();
});
